#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from simplematrixlib import download, parse_mxc, get_config

description = """
Download an mxc URI and save the file locally.
"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver'],
                        noparse=True, config_name='py-matrix-utils')

    config.add('mxc', help="MXC URI to download")
    config = config.parse()

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    mxc = config.mxc
    while not mxc:
        mxc = input("MXC URI: ")

    serverName, mediaId = parse_mxc(mxc)

    file = download(homeserver, serverName, mediaId)

    with open(file['filename'], 'wb') as outfile:
        outfile.write(file['content'])
