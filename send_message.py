#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import getpass
from simplematrixlib import (
    send_message, resolve_room_alias, is_room_id, get_config
)

description = """
Send a message to a matrix room
"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver', 'username',
                        'access_token'], noparse=True,
                        config_name='py-matrix-utils')

    config.add('-f', '--formatted', help="\
               Add a formatted body, for sending HTML")
    config.add('-r', '--room', required=True, help="Room to send message in")
    config.add('body', help="Message body")
    config = config.parse()

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    username = config.username
    while not username:
        username = input("Username or full ID: ")

    access_token = config.access_token
    while not access_token:
        access_token = getpass("Enter access token: ")

    room = config.room
    while not room:
        room = input("Room to send message in: ")
    if not is_room_id(room):
        room = resolve_room_alias(homeserver, room)

    body = config.body
    while not body:
        body = input("Message to send: ")

    formatted_body = config.formatted

    if formatted_body:
        format = 'org.matrix.custom.html'

    request = send_message(homeserver, access_token, room, body,
                           formatted_body=formatted_body, format=format)
