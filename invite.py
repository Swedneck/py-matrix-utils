#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from simplematrixlib import get_config, invite, resolve_room_alias
from time import sleep

description = """

"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver', 'access_token'],
                        noparse=True,
                        config_name='py-matrix-utils')

    config.add('room_alias', nargs='?', help="Room to invite user(s) to")
    config.add('user_id', nargs='?', help="User to invite")
    config.add('-m', '--multiple', required=False, help="Listen on stdin for a\
            space seprated list of user IDs to invite", action='store_true')
    config = config.parse()

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    access_token = config.access_token
    if not access_token:
        print("Please supply an access token.")

    room = config.room_alias
    while not room:
        room = input("Room to invite user(s) to: ")

    if room[0] == '!':
        room_id = room
    else:
        room_id = resolve_room_alias(homeserver, room)

    if config.multiple:
        user_ids = input().split()
        for user in user_ids:
            print(invite(homeserver, access_token, room_id, user)['error'])
            sleep(0.5)
    else:
        user_id = config.user_id
        while not user_id:
            user_id = input("User to invite to room (full ID): ")
        print(invite(homeserver, access_token, room_id, user_id)['error'])
