#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from simplematrixlib import get_config, resolve_room_alias, get_room_members

description = """
Returns a sorted list of all members in a room.
"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver',
                        'access_token', 'room_alias'],
                        config_name='py-matrix-utils')

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    access_token = config.access_token
    while not access_token:
        access_token = input("Access token: ")

    room_alias = config.room_alias
    while not room_alias:
        room_alias = input("Room alias: ")

    room_id = resolve_room_alias(homeserver, room_alias)

    room_members = get_room_members(homeserver, access_token, room_id)
    for key in room_members:
        print(key)
