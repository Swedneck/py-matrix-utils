import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="py-matrix-utils",
    version="0.2.0",
    author="Tim Stahel",
    author_email="pymatrixutils@swedneck.xyz",
    description="A set of utlities for the Matrix chat protocol",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Swedneck/py-matrix-utils",
    packages=setuptools.find_packages(),
    install_requires=[
        'simplematrixlib',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Topic :: Communications :: Chat",
        "License :: OSI Approved :: GNU Affero General Public License v3",
    ],
    python_requires='>=3.7',
)
