#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
from simplematrixlib import get_config, api

description = """
Wrapper for simplematrixlib.api
"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver', 'access_token'],
                        noparse=True, config_name='py-matrix-utils')

    config.add('method', help="Which method to use")
    config.add('endpoint', help="The endpoint to call")
    config.add('-D', '--data', help="Data to send")
    config.add('-P', '--parameters', help="Parameters to send")
    config.add('-H', '--headers', help="Headers to send")
    config = config.parse()

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    access_token = config.access_token
    while not access_token:
        access_token = input("Access token: ")

    method = config.method
    endpoint = config.endpoint
    data = json.loads(config.data)
    params = config.parameters
    headers = config.headers

    # run api.{method}
    response = getattr(api, method)(
                endpoint, homeserver, access_token=access_token, data=data,
                params=params, headers=headers)

    print(response)
    print(response.json())
