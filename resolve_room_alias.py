#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from simplematrixlib import get_config, resolve_room_alias

description = """
Resolves a room alias to a room ID.
"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver'],
                        noparse=True, config_name='py-matrix-utils')

    config.add('alias', help="The alias to resolve")
    config = config.parse()

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    alias = config.alias
    while not alias:
        alias = input("Room alias to resolve: ")

    print(resolve_room_alias(homeserver, alias))
