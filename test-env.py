#!/bin/env python3
import simplematrixlib as matrix
from simplematrixlib import api_call
from simplematrixlib import api

config = matrix.get_config('', options=['homeserver', 'username',
                           'access_token'])
homeserver = config.homeserver
username = config.username
access_token = config.access_token
