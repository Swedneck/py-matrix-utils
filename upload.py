#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from getpass import getpass
from simplematrixlib import upload, get_config

description = """
Upload a file and output the mxc URI
"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver', 'username',
                        'access_token'], noparse=True,
                        config_name='py-matrix-utils')

    config.add('file', help="Path to the file to be uploaded")
    config = config.parse()

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    username = config.username
    while not username:
        username = input("Username or full ID: ")

    access_token = config.access_token
    while not access_token:
        access_token = getpass("Enter access token: ")

    file = config.file
    while not file:
        file = input("File path: ")

    uri = upload(homeserver, access_token, file)
    print(uri['content_uri'])
