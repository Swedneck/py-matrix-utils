#!/bin/env python3
#   Copyright (C) 2019  Tim Stahel
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from getpass import getpass
from simplematrixlib import login, get_config

description = """
Takes a username and password and outputs an access token.
Password is required to be entered manually, to prevent leakage.

"""

if __name__ == '__main__':
    config = get_config(description, options=['homeserver', 'username'],
                        config_name='py-matrix-utils')

    homeserver = config.homeserver
    while not homeserver:
        homeserver = input("URL of your homeserver: ")

    username = config.username
    while not username:
        username = input("Username or full ID: ")

    password = getpass()
    while not password:
        password = getpass()

    print("Access Token:")
    print(login(homeserver, username, password))
