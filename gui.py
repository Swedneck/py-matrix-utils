#!/bin/env python3
import simplematrixlib as matrix
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

description = """
Matrix client based on simplematrixlib.
"""

config = matrix.get_config(description,
                           options=[
                                    'homeserver',
                                    'username',
                                    'access_token'
                                   ],
                           config_name='py-matrix-utils')

homeserver = config.homeserver
username = config.username
access_token = config.access_token


def get_room():
    roomEntry = builder.get_object("roomEntry")
    room = roomEntry.get_text()
    print(room)
    if not matrix.is_room_id(room):
        room = matrix.resolve_room_alias(homeserver, room)


def send_message(self):
    room = get_room()
    buffer = messageBox.get_buffer()
    message = buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(),
                              False)
    result = matrix.send_message(homeserver, access_token, room, message)
    print(result)


def list_users(self):
    room = get_room()
    print(room)
    users = matrix.get_room_members(homeserver, access_token, room)
    userList = builder.get_object("userList")
    userList.get_buffer().set_text(users)


if __name__ == '__main__':

    builder = Gtk.Builder()
    builder.add_from_file("gui.ui")

    window = builder.get_object("window")
    window.connect("destroy", Gtk.main_quit)
    window.show_all()

    roomEntry = builder.get_object("roomEntry")

    messageBox = builder.get_object("messageBox")
    sendMessage = builder.get_object("sendMessage")
    listUserButton = builder.get_object("listUsersButton")

    sendMessage.connect("clicked", send_message)
    listUserButton.connect("clicked", list_users)

    Gtk.main()
