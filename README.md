A collection of python scripts that make it easier to interact with the [Matrix
protocol](https://matrix.org) via the commandline, using simplematrixlib.

Beware that I'm a beginner python user, any tips or pull requests are very
welcome.


# Requirements
Install `simplematrixlib`, easiest via `pip install --user simplematrixlib`.


https://gitlab.com/Swedneck/py-matrix-utils
